package br.com.confidencecambio.javabasico.process;

import br.com.confidencecambio.javabasico.serviceImpl.UtilsStringServiceImpl;
import br.com.confidencecambio.javabasico.vo.ClienteVO;
import br.com.confidencecambio.javabasico.vo.GerenteVO;
import br.com.confidencecambio.javabasico.vo.RetornoVO;
import br.com.confidencecambio.javabasico.vo.RoboVO;

public class UtilsStringProcess {

    public RetornoVO validaStringNull(ClienteVO clienteVO) {
    	UtilsStringServiceImpl utilsStringServiceImpl = new UtilsStringServiceImpl();
    	
    	RetornoVO retornoVO = new RetornoVO();
    	
    	retornoVO = utilsStringServiceImpl.validaStringNull(clienteVO);

		return retornoVO;
    }
    
    public RetornoVO retornaPrimeiroNome(GerenteVO gerenteVO) {
    	UtilsStringServiceImpl utilsStringServiceImpl = new UtilsStringServiceImpl();
    	
    	RetornoVO retornoVO = new RetornoVO();
    	
    	retornoVO = utilsStringServiceImpl.retornaPrimeiroNome(gerenteVO);

		return retornoVO;
    }
    
    public RetornoVO retornaSobreNome(RoboVO roboVO) {
    	UtilsStringServiceImpl utilsStringServiceImpl = new UtilsStringServiceImpl();
    	
    	RetornoVO retornoVO = new RetornoVO();
    	
    	retornoVO = utilsStringServiceImpl.retornaSobreNome(roboVO);

		return retornoVO;
    }
    
    
    public RetornoVO retornaLetrasMaisculas(RoboVO roboVO) {
    	UtilsStringServiceImpl utilsStringServiceImpl = new UtilsStringServiceImpl();
    	
    	RetornoVO retornoVO = new RetornoVO();
    	
    	retornoVO = utilsStringServiceImpl.retornaLetrasMaisculas(roboVO);

		return retornoVO;
    }
    
    public RetornoVO retornaSobreNomeAbreviado(RoboVO roboVO) {
    	UtilsStringServiceImpl utilsStringServiceImpl = new UtilsStringServiceImpl();
    	
    	RetornoVO retornoVO = new RetornoVO();
    	
    	retornoVO = utilsStringServiceImpl.retornaSobreNomeAbreviado(roboVO);

		return retornoVO;
    }
    
}
