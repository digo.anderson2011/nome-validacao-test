package br.com.confidencecambio.javabasico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import br.com.confidencecambio.javabasico.process.UtilsStringProcess;
import br.com.confidencecambio.javabasico.vo.ClienteVO;
import br.com.confidencecambio.javabasico.vo.GerenteVO;
import br.com.confidencecambio.javabasico.vo.RetornoVO;
import br.com.confidencecambio.javabasico.vo.RoboVO;

@SpringBootApplication
public class JavaBasicoApplication {

	public static void main(String[] args) {
		UtilsStringProcess utilsStringProcess = new UtilsStringProcess();
		
		ClienteVO clienteVO = new ClienteVO();
		clienteVO.setNome("Anderson Soares	");
		RetornoVO retornoVO = utilsStringProcess.validaStringNull(clienteVO);
		
		System.out.println("");
    	System.out.println("◦Nome não pode ser nulo: / vazio , retira espaços extras no início e no fim");
    	System.out.println("Entrada: " +retornoVO.getMensagem());
    	System.out.println("Resultado: " +retornoVO.getMensagem());
    	
    	
    	System.out.println("");
    	System.out.println("");
    	
    	System.out.println("##############################################################################");
    	System.out.println("");

    	GerenteVO gerenteVO = new GerenteVO();
    	gerenteVO.setNome("João Soares Silva");
		RetornoVO retornoGerenteVO = utilsStringProcess.retornaPrimeiroNome(gerenteVO);
		
    	
    	System.out.println("◦Deve ser possível obter o primeiro nome. Exemplo: Se o nome for \"João Soares Silva\", deve retornar \"João\".");
    	System.out.println("Entrada: "+gerenteVO.getNome());
    	System.out.println("Resultado: "+retornoGerenteVO.getMensagem());
    	
    	
    	System.out.println("");
    	
    	System.out.println("##############################################################################");
    	System.out.println("");

    	RoboVO roboVO = new RoboVO();
    	roboVO.setNome("João Soares Silva");
		RetornoVO retornoRoboVO = utilsStringProcess.retornaSobreNome(roboVO);
		
    	
    	System.out.println("◦◦Retornar o último nome. Exemplo: Se o nome for \"João Soares Silva\", deve retornar \"Soares Silva\".");
    	System.out.println("Entrada: " + roboVO.getNome());
    	System.out.println("Resultado: " + retornoRoboVO.getMensagem());
    	
    	
    	System.out.println("");
    	
    	System.out.println("##############################################################################");
    	System.out.println("");

    	RoboVO roboMaVO = new RoboVO();
    	roboMaVO.setNome("João Soares Silva");
		RetornoVO retornoRoboMaVO = utilsStringProcess.retornaLetrasMaisculas(roboVO);
		
    	
    	System.out.println("◦Retornar o nome todo em letras maiúsculas");
    	System.out.println("Entrada: " +roboMaVO.getNome() );
    	System.out.println("Resuldado: " +retornoRoboMaVO.getMensagem());
    	
    	
    	
    	
    	System.out.println("");
    	
    	System.out.println("##############################################################################");
    	System.out.println("");

    	RoboVO roboAprVO = new RoboVO();
    	roboAprVO.setNome("João Soares Silva");
		RetornoVO retornoRoboMaAprVO = utilsStringProcess.retornaSobreNomeAbreviado(roboVO);
		
    	
    	System.out.println("◦Retornar o nome abreviado. Exemplo: Se o nome for \"João Soares Silva\", retornar \"João S. Silva\".");
    	System.out.println("Entrada: " + roboAprVO.getNome());
    	System.out.println("Resultado: " + retornoRoboMaAprVO.getMensagem());
    	
    	
	}

}
