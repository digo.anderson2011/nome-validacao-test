package br.com.confidencecambio.javabasico.serviceImpl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Strings;

import br.com.confidencecambio.javabasico.service.UtilsStringServiceInterface;
import br.com.confidencecambio.javabasico.vo.ClienteVO;
import br.com.confidencecambio.javabasico.vo.GerenteVO;
import br.com.confidencecambio.javabasico.vo.RetornoVO;
import br.com.confidencecambio.javabasico.vo.RoboVO;

public class UtilsStringServiceImpl implements UtilsStringServiceInterface {

	private static final String MESSAGEM_NULL_VAZIO = "campo obrigatório - Nome do Cliente, não pode ser null ou vazio";

	public RetornoVO validaStringNull(ClienteVO clienteVO) {

		RetornoVO retornoVO = new RetornoVO();

		if (!Strings.nullToEmpty(clienteVO.getNome()).trim().isEmpty()) {
			retornoVO.setMensagem(clienteVO.getNome().trim());

		} else {

			retornoVO.setMensagem(MESSAGEM_NULL_VAZIO);
		}

		return retornoVO;
	}

	public RetornoVO retornaPrimeiroNome(GerenteVO GerengteVO) {

		RetornoVO retornoVO = new RetornoVO();

		String pattern = "\\S+";

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(GerengteVO.getNome());
		if (m.find()) {
			retornoVO.setMensagem(m.group(0));
		}

		return retornoVO;
	}

	public RetornoVO retornaSobreNome(RoboVO roboVO) {

		RetornoVO retornoVO = new RetornoVO();

		String[] arr = roboVO.getNome().split("\\s");
		int count = 0;
		String mensagem = "";

		for (String ch : arr) {
			if (0 != count) {
				mensagem = mensagem + ch + " ";
				retornoVO.setMensagem(mensagem);
			}
			count++;
		}

		return retornoVO;
	}

	public RetornoVO retornaLetrasMaisculas(RoboVO roboVO) {

		RetornoVO retornoVO = new RetornoVO();

		retornoVO.setMensagem(roboVO.getNome().toUpperCase());

		return retornoVO;
	}

	public RetornoVO retornaSobreNomeAbreviado(RoboVO roboVO) {

		RetornoVO retornoVO = new RetornoVO();

		String[] arr = roboVO.getNome().split("\\s");
		int count = 0;
		String mensagem = "";

		for (String ch : arr) {
			if (1 == count) {
				mensagem = mensagem + ch.substring(0, 1) + "." + " ";
			} else {
				mensagem = mensagem + ch + " ";
			}

			retornoVO.setMensagem(mensagem);
			count++;
		}

		return retornoVO;
	}
}
