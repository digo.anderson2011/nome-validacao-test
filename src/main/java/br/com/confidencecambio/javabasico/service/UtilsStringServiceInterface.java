package br.com.confidencecambio.javabasico.service;

import br.com.confidencecambio.javabasico.vo.ClienteVO;
import br.com.confidencecambio.javabasico.vo.GerenteVO;
import br.com.confidencecambio.javabasico.vo.RetornoVO;
import br.com.confidencecambio.javabasico.vo.RoboVO;

public interface UtilsStringServiceInterface {

	public RetornoVO validaStringNull(ClienteVO clienteVO);
	public RetornoVO retornaPrimeiroNome(GerenteVO GerenteVO);
	public RetornoVO retornaSobreNome(RoboVO roboVO);
	public RetornoVO retornaLetrasMaisculas(RoboVO roboVO);
	public RetornoVO retornaSobreNomeAbreviado(RoboVO roboVO);
	
}
