/**
 *
 */
package br.com.confidencecambio.javabasico.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Anderson Soares
 * @version 1.0
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClienteVO implements Serializable {

  private static final long serialVersionUID = 6309597593630150709L;

  @NotNull(message ="obrigatorio")
  private String nome;
}
